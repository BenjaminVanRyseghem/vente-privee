/*!
 * Copyright (c) 2017 Benjamin Van Ryseghem<benjamin@vanryseghem.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import Constants from "../src/js/Constants";
import AppDispatcher from "../src/js/Dispatcher";
import HeroStore from "../src/js/stores/HeroStore";
import fetchMock from "fetch-mock";

jest.mock("../src/js/Dispatcher", () => ({
	register: jest.fn()
}));

describe("HeroStore", function() {
	let hero = {id: "foo"};
	let heroes = [hero, {id: "bar"}];

	// mock actions inside dispatch payloads
	let actionHeroLoaded = {
		type: Constants.ActionTypes.HERO_LOADED,
		hero
	};

	let actionAllHeroLoaded = {
		type: Constants.ActionTypes.ALL_HEROES_LOADED,
		heroes
	};

	let callback;

	beforeEach(function() {
		callback = AppDispatcher.register.mock.calls[0][0];
	});

	afterEach(() => {
		fetchMock.restore();
	});

	it("registers a callback with the dispatcher", function() {
		expect(AppDispatcher.register.mock.calls.length).toBe(1);
	});

	it("initializes with no heroes", function() {
		let all = HeroStore.getAll();
		expect(all).toEqual({heroes: []});
	});

	it("loads a hero", function() {
		fetchMock.get("*", JSON.stringify({data: {results: [hero]}}));
		callback(actionHeroLoaded);
		let actualHero = HeroStore.get("foo").hero;
		expect(actualHero).toEqual(hero);
	});

	it("loads all heroes", function() {
		callback(actionAllHeroLoaded);
		let actualHeroes = HeroStore.getAll().heroes;
		expect(actualHeroes[0].id).toEqual("foo");
		expect(actualHeroes[1].id).toEqual("bar");
	});
});
