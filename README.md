
# vente-privee

[![pipeline status](https://gitlab.com/BenjaminVanRyseghem/vente-privee/badges/master/pipeline.svg)](https://gitlab.com/BenjaminVanRyseghem/vente-privee/commits/master)

> React exercise for vente-privee

## Running your project

This project includes a live-reloading static server on port `8080` (you can change the port in the `gulpfile.js` config), which will build, launch, and rebuild the app whenever you change application code. To start the server, run:

```bash
$ npm start
```

If you prefer to just build without the live reload and build-on-each-change watcher, run:

```bash
$ npm run build
```

To run tests:

```bash
$ npm test
```
