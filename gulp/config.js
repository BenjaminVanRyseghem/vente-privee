/*!
 * Copyright (c) 2017 Benjamin Van Ryseghem<benjamin@vanryseghem.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

let dest = "./dist";
let src = "./src";
let gutil = require("gulp-util");

module.exports = {
	server: {
		settings: {
			root: dest,
			host: "localhost",
			port: 8080,
			livereload: {
				port: 35929
			}
		}
	},
	sass: {
		src: src + "/styles/css/**/*.{sass,scss,css}",
		dest: dest + "/styles/css",
		settings: {
			indentedSyntax: false,
			imagePath: "/images"
		}
	},
	fontawesome: {
		src: "node_modules/font-awesome/{css,fonts}/**/*",
		dest: dest + "/styles"
	},
	fonts: {
		src: src + "/styles/fonts/**/*",
		dest: dest + "/styles/fonts"
	},
	browserify: {
		settings: {
			transform: ["babelify", "reactify"]
		},
		src: src + "/js/index.jsx",
		dest: dest + "/js",
		outputName: "index.js",
		debug: gutil.env.type === "dev"
	},
	html: {
		src: "src/index.html",
		dest: dest
	},
	watch: {
		src: "src/**/*.*",
		tasks: ["build"]
	}
};
