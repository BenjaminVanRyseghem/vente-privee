/*!
 * Copyright (c) 2017 Benjamin Van Ryseghem<benjamin@vanryseghem.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import Dispatcher from "../Dispatcher";
import Constants from "../Constants";
import BaseStore from "./BaseStore";
import crypto from "crypto-browserify";

let _cache = {};

function addHeroes(heroes) {
	heroes.forEach((hero) => {
		_cache[hero.id] = hero;
	});
}

function addHero(hero) {
	_cache[hero.id] = hero;
}

function buildParams() {
	let now = Date.now();
	let apiKey = "298bab46381a6daaaee19aa5c8cafea5";
	let apiSecret = "b0223681fced28de0fe97e6b9cd091dd36a5b71d";
	let hash = crypto.createHash("md5").update(`${now}${apiSecret}${apiKey}`).digest("hex");
	return `ts=${now}&apikey=${apiKey}&hash=${hash}`;
}

const HeroStore = Object.assign({}, BaseStore, {
	fetchAll() {
		return fetch(`http://gateway.marvel.com:80/v1/public/characters?${buildParams()}`, {
			method: "get",
			headers: {
				"Content-Type": "application/json"
			}
		}).then(
			response => response.json(),
			() => console.error(...arguments) // eslint-disable-line no-console
		).then((jsonData) => {
			let data = jsonData.data.results;
			Dispatcher.dispatch({
				type: Constants.ActionTypes.ALL_HEROES_LOADED,
				heroes: data
			});
		});
	},

	fetch(id) {
		return fetch(`http://gateway.marvel.com:80/v1/public/characters/${id}?${buildParams()}`, {
			method: "get",
			headers: {
				"Content-Type": "application/json"
			}
		}).then(
			response => response.json(),
			() => console.error(...arguments) // eslint-disable-line no-console
		).then((jsonData) => {
			let data = jsonData.data.results[0];
			Dispatcher.dispatch({
				type: Constants.ActionTypes.HERO_LOADED,
				hero: data
			});
		});
	},

	get(id) {
		let hero = _cache[id];

		if (!hero) {
			this.fetch(id);
		}

		return {hero: hero};
	},

	getAll() {
		return {heroes: Object.values(_cache)};
	},

	// register store with dispatcher, allowing actions to flow through
	dispatcherIndex: Dispatcher.register(function handleAction(action) {
		switch (action.type) { // eslint-disable-line default-case
			case Constants.ActionTypes.ALL_HEROES_LOADED:
				let heroes = action.heroes;
				addHeroes(heroes);
				HeroStore.emitChange();
				break;
			case Constants.ActionTypes.HERO_LOADED:
				let hero = action.hero;
				addHero(hero);
				HeroStore.emitChange();
				break;
		}
	})
});

export default HeroStore;
