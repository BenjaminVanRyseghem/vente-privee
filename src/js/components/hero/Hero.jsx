/*!
 * Copyright (c) 2017 Benjamin Van Ryseghem<benjamin@vanryseghem.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from "react";
import HeroStore from "../../stores/HeroStore";
import Spinner from "../Spinner.jsx"; // eslint-disable-line no-unused-vars

const collectionRenderer = (title, collection) => {
	if (!collection || !collection.available) {
		return "";
	}

	return <li>
		<span className="collection-title">{title}</span>
		<ul className="collection-entries">
			{collection.items.map((item) => {
				return <li className="collection-entry">{item.name}</li>;
			})}
		</ul>
	</li>;
};

class Hero extends React.Component {
	constructor(props) {
		super(props);
		let newState = HeroStore.get(props.id);
		if (!newState.hero) {
			newState.loading = true;
		}

		this.state = newState;
	}

	_onChange() {
		let newState = HeroStore.get(this.props.id);
		newState.loading = false;
		this.setState(newState);
	}

	componentDidMount() {
		HeroStore.addChangeListener(this._onChange.bind(this));
	}

	componentWillUnmount() {
		HeroStore.removeChangeListener(this._onChange.bind(this));
	}

	render() {
		let {hero, loading} = this.state;
		if (loading) {
			return <Spinner/>;
		}

		return (<div className="hero-bio">
			<div className="hero-avatar">
				<img
					className="visible-sm visible-md visible-lg"
					src={`${hero.thumbnail.path}.${hero.thumbnail.extension}`}
				/>
			</div>
			<div className="hero-info">
				<div className="hero-id">
					<h3>{hero.name}</h3>
					{hero.description &&
					<p className="description">
						{hero.description}
					</p>
					}
				</div>
				<ul>
					{collectionRenderer("Comics", hero.comics)}
					{collectionRenderer("Series", hero.series)}
					{collectionRenderer("Story", hero.stories)}
					{collectionRenderer("Events", hero.events)}
				</ul>
			</div>
		</div>);
	}
}

export default Hero;
