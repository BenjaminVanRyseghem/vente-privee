/*!
 * Copyright (c) 2017 Benjamin Van Ryseghem<benjamin@vanryseghem.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from "react";
import HeroPreview from "./HeroPreview.jsx"; // eslint-disable-line no-unused-vars
import PropTypes from "prop-types";

function heroesSorting(a, b) {
	return a.name <= b.name ? -1 : 1;
}

class IndexContent extends React.Component {
	render() {
		let {heroes} = this.props;
		let heroesList = "No heroes found!"; // eslint-disable-line no-unused-vars

		if (heroes && heroes.length) {
			heroesList = <ul>
				{heroes.sort(heroesSorting).map((datum) => {
					return <HeroPreview hero={datum}/>;
				})}
			</ul>;
		}
		return (
			<div className="container">
				{heroesList}
			</div>
		);
	}
}

IndexContent.propTypes = {
	heroes: PropTypes.array.isRequired
};

IndexContent.defaultProps = {
	heroes: []
};

export default IndexContent;
