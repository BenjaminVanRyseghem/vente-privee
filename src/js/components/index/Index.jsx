/*!
 * Copyright (c) 2017 Benjamin Van Ryseghem<benjamin@vanryseghem.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from "react";
import HeroStore from "../../stores/HeroStore";
import Spinner from "../Spinner.jsx"; // eslint-disable-line no-unused-vars
import IndexContent from "./IndexContent.jsx"; // eslint-disable-line no-unused-vars

class Index extends React.Component {
	constructor(props) {
		super(props);
		HeroStore.fetchAll();
		this.state = {loading: true};
	}

	_onChange() {
		let newState = HeroStore.getAll();
		newState.loading = false;
		this.setState(newState);
	}

	componentDidMount() {
		HeroStore.addChangeListener(this._onChange.bind(this));
	}

	componentWillUnmount() {
		HeroStore.removeChangeListener(this._onChange.bind(this));
	}

	render() {
		let {loading, heroes} = this.state;
		if (loading) {
			return <Spinner/>;
		}

		return (
			<IndexContent heroes={heroes}/>
		);
	}
}

export default Index;
