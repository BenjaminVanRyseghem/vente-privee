/*!
 * Copyright (c) 2017 Benjamin Van Ryseghem<benjamin@vanryseghem.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from "react";
import {Link} from "react-router-dom"; // eslint-disable-line no-unused-vars

function findActions(hero) {
	let urls = hero.urls;
	let detail = urls.find((url) => url.type === "detail");
	let wiki = urls.find((url) => url.type === "wiki");
	let comiclink = urls.find((url) => url.type === "comiclink");
	return [detail, wiki, comiclink];
}

const actionRenderer = (name, data) => {
	if (!data) {
		return "";
	}

	return <span className="action">
		<a href={data.url}>
			<i className="fa fa-book"/>
			{name}
		</a>
	</span>;
};


class HeroPreview extends React.Component {
	render() {
		let {hero} = this.props;
		let [detail, wiki, comiclink] = findActions(hero);

		return (
			<div className="col-sm-3 hero-card">
				<div className="card-container">
					<div className="hero-avatar">
						<img
							src={`${hero.thumbnail.path}.${hero.thumbnail.extension}`}
						/>
					</div>
					<div className="hero-name" title={hero.name}><Link
						to={`/hero/${hero.id}`}>{hero.name}</Link></div>
					<div className="actions">
						{actionRenderer("details", detail)}
						{actionRenderer("wiki", wiki)}
						{actionRenderer("comic", comiclink)}
					</div>
				</div>
			</div>
		);
	}
}

export default HeroPreview;
