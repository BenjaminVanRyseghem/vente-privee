/*!
 * Copyright (c) 2017 Benjamin Van Ryseghem<benjamin@vanryseghem.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from "react"; // eslint-disable-line no-unused-vars
import {HashRouter as Router, Route} from "react-router-dom"; // eslint-disable-line no-unused-vars
import ReactDom from "react-dom";
import ScrollToTop from "./ScrollToTop.jsx"; // eslint-disable-line no-unused-vars
import Index from "./components/index/Index.jsx"; // eslint-disable-line no-unused-vars
import Hero from "./components/hero/Hero.jsx"; // eslint-disable-line no-unused-vars
import Nav from "./components/nav/Nav.jsx"; // eslint-disable-line no-unused-vars
import "isomorphic-fetch";

const HeroDispatcher = ({match}) => (
	<Hero id={match.params.id}/>
);

const Dispatcher = () => { // eslint-disable-line no-unused-vars
	return <div>
		<Nav/>
		<div className="wrapper">
			<Route exact path="/" component={Index}/>
			<Route path="/hero/:id" component={HeroDispatcher}/>
		</div>
	</div>;
};

ReactDom.render((
	<Router onUpdate={() => window.scrollTo(0, 0)}>
		<ScrollToTop>
			<Dispatcher/>
		</ScrollToTop>
	</Router>
), document.getElementById("main"));

